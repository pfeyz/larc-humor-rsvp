export function getKey(e: KeyboardEvent): string {
    var keynum;

    if (window.event) { // IE
        keynum = e.keyCode;
    } else if (e.which) { // Netscape/Firefox/Opera
        keynum = e.which;
    }

    return String.fromCharCode(keynum).toLowerCase();
}

export function delay(duration): Promise<never> {
    let prevTime = performance.now();
    return new Promise((resolve, reject) => {
        let animationLoop = (time: number) => {
            var req = window.requestAnimationFrame(animationLoop);
            if (performance.now() - prevTime > duration) {
                window.cancelAnimationFrame(req);
                resolve();
            }
        }
        window.requestAnimationFrame(animationLoop);
    });
}

export function durationalText(element, text, duration) {
    element.innerHTML = text;
    return delay(duration)
        .then(() => { element.innerHTML = ''; });
}

export let fixationCross = (element, duration) => durationalText(element, '+', duration);

export function shuffle<T>(a: T[]) {
    for (let i = a.length; i; i--) {
        let j = Math.floor(Math.random() * i);
        [a[i - 1], a[j]] = [a[j], a[i - 1]];
    }
}

var cached = [];
export function preloadImages(prefix: string, images: string[]) {
    window.setTimeout(() => {
        cached = cached.concat(
            images.map(filename => {
                var image = new Image();
                image.src = `${prefix}/${filename}`;
                return image;
            }))
    }, 1);
}

export function uuid(): string {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

function getStoredJson(key): {} {
    return JSON.parse(localStorage.getItem(key)) || {};
}

function storeJson(key, data) {
    localStorage.setItem(key, JSON.stringify(data));
}

function getQueryParams(): {} {
    let qString = window.location.search,
        queryRe = /[?&]([^=]+?)=([^?]+?)(?=&|$)/g,
        triple = null,
        params = {};

    while (triple = queryRe.exec(qString)) {
        let [_, key, val] = triple;
        params[key] = val;
    }

    return params;
}

export interface UserMetadata {
    participantId: string; // randomly assigned on each refresh unless set via
    // query parameter.
    sessionId: string;     // changes on every browser refresh
    browserId: string;     // this persists across experimental runs
}

export function getUserMetadata(): UserMetadata {
    let location = 'metadata',
        queryParams = getQueryParams(),
        storedParams = getStoredJson(location),
        metadata = {
            'participantId': queryParams['id'] || uuid(),
            'browserId': storedParams['browserId'] || uuid(),
            'sessionId': window['sessionId'] || uuid()
        };

    window['sessionId'] = metadata['sessionId'];

    storeJson(location, metadata);

    return metadata;
}
