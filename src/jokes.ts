export var jokes = {
    "practice": [
        {
            "joke": "When my boss asked me who is the stupid one, me or him? I told him everyone knows he doesn't hire stupid people.",
            "nonJoke": "When my boss asked me who is the stupid one, me or him? I told him everyone knows we are both smart.",
            "id": "p0"
        },
        {
            "joke": "Everything is edible, some things are only edible once.",
            "nonJoke": "Everything is edible, some things are even delicious.",
            "id": "p1"
        },
        {

            "joke": "I'm an archaeologist and my life is in ruins.",
            "nonJoke": "I'm an archaeologist and my life is in travelling.",
            "id": "p2"
        },
        {
            "joke": "Does the name Pavlov ring a bell?",
            "nonJoke": "Does the name Pavlov sound familiar?",
            "id": "p3"
        },
        {

            "joke": "A life in politics is full of parties.",
            "nonJoke": "A life in politics is full of work.",
            "id": "p4"
        }
    ],
    "experimental": [
        {
            "joke": "How long have I been working for the company? Ever since they threatened to fire me.",
            "nonJoke": "How long have I been working for the company? Since 2015.",
            "id": "e0"
        },
        {
            "joke": "Apparently I snore so loudly that it scares everyone in the car I'm driving.",
            "nonJoke": "Apparently I snore so loudly that it scares everyone in the house I sleep in.",
            "id": "e1"
        },
        {
            "joke": "If someone hates you for no reason, give them a reason.",
            "nonJoke": "If someone hates you for no reason, just take it easy.",
            "id": "e2"

        },
        {
            "joke": "Every paper towel commercial reminds me that the cleanest solution is to just not have children.",
            "nonJoke": "Every paper towel commercial reminds me that the cleanest solution is just not to spill.",
            "id": "e3"
        },
        {
            "joke": "I asked to switch seats on a plane because I was sat next to a crying. Apparently that's not allowed if it's yours.",
            "nonJoke": "I asked to switch seats on a plane because I was seated next to a crying baby. Apparently that's not allowed if all the seats are taken.",
            "id": "e4"
        },
        {
            "joke": "I can't believe I forgot to go to the gym today. That's 7 years in a row now.",
            "nonJoke": "I can't believe I forgot to go to the gym today. I'll make up for it tomorrow.",
            "id": "e5"
        },
        {
            "joke": "Entered what I ate today into my new fitness app and it just sent an ambulance to my house.",
            "nonJoke": "Entered what I ate today into my new fitness app and it calculated 2000 calories.",
            "id": "e6"
        },
        {
            "joke": "My  dog used to chase people on a bike a lot. It got so bad, finally, I had to take the bike away.",
            "nonJoke": "My  dog used to chase people on a bike a lot. It got so bad, I finally had to keep her in the house.",
            "id": "e7"
        },
        {
            "joke": "Nothing ruins a Friday more than an understanding that today is Tuesday.",
            "nonJoke": "Nothing ruins a Friday more than an understanding that the weather will be bad on the weekend.",
            "id": "e8"
        },
        {
            "joke": "I like to hold hands at the movies\u2026 which always seems to startle strangers.",
            "nonJoke": "I like to hold hands at the movies and my boyfriend likes it, too.",
            "id": "e9"
        },
        {
            "joke": "When my boss told me this is the fifth time I'm late, I smiled and thought to myself, it's Friday!",
            "nonJoke": "When my boos told me this is the fifth time I'm late, I was embarrassed.",
            "id": "e10"
        },
        {
            "joke": "Nothing makes me more productive than the last minute.",
            "nonJoke": "Nothing makes me more productive than a cup of coffee.",
            "id": "e11"
        },
        {
            "joke": "The depressing thing about tennis is that no matter how good I get, I'll never be as good as a wall.",
            "nonJoke": "The depressing thing about tennis is that no matter how good I get, I'll never be as good as Serena Williams.",
            "id": "e12"
        },
        {
            "joke": "With great power comes great electricity bills.",
            "nonJoke": "With great power comes great responsibility.",
            "id": "e13"
        },
        {
            "joke": "My teacher said two positives can never make a negative, so I was all like \"yeah, right.\"",
            "nonJoke": "My teacher said two positives can never make a negative, so I wrote it down.",
            "id": "e14"
        },
        {
            "joke": "It is so cold outside. I saw a politician with hands in his own pockets.",
            "nonJoke": "It is so cold outside. I saw a politician with hands in his gloves.",
            "id": "e15"
        },
        {
            "joke": "I am so poor I can't even pay attention.",
            "nonJoke": "I am so poor I can't even pay for food.",
            "id": "e16"
        },
        {
            "joke": "I started with nothing, and I still have most of it.",
            "nonJoke": "I started with nothing, and I now I am at the top of my field.",
            "id": "e17"
        },
        {
            "joke": "The biggest lie I was told in school was that I wouldn't always have a calculator with me.",
            "nonJoke": "The biggest lie I was told in school was that I would need all the subjects they teach.",
            "id": "e18"
        },
        {
            "joke": "A clean house is the sign of a broken computer.",
            "nonJoke": "A clean house is the sign of neat people.",
            "id": "e19"
        },
        {
            "joke": "Behind every great man is a woman rolling her eyes.",
            "nonJoke": "Behind every great man is a great woman.",
            "id": "e20"
        },
        {
            "joke": "Every time I find the meaning of life, they change it.",
            "nonJoke": "Every time I find the meaning of life, I get very happy.",
            "id": "e21"
        },
        {
            "joke": "Just the thought of having insomnia keeps me awake at night.",
            "nonJoke": "Just the thought of having insomnia makes me upset.",
            "id": "e22"
        },
        {
            "joke": "I always arrive late to work, but I make up for it by leaving early.",
            "nonJoke": "I always arrive late to work, but I make up for it by staying late.",
            "id": "e23"
        },
        {
            "joke": "If two people on opposite sides of the world each drop a piece of bread, the Earth briefly becomes a sandwich.",
            "nonJoke": "If two people on opposite sides of the world each drop a piece of bread, it falls to the floor.",
            "id": "e24"
        },
        {
            "joke": "A bank is a place that will lend you money, if you can prove that you don't need it.",
            "nonJoke": "A bank is a place that will lend you money, if you provide the necessary documents.",
            "id": "e25"
        },
        {
            "joke": "Insanity is hereditary. You get it from your kids.",
            "nonJoke": "Insanity is hereditary. You get it from your parents.",
            "id": "e26"
        },
        {
            "joke": "I'm in shape. Round is a shape, isn't it?",
            "nonJoke": "I'm in shape. I work out.",
            "id": "e27"
        },
        {
            "joke": "Today a man knocked on my door and asked for a small donation towards the local swimming pool. I gave him a glass of water.",
            "nonJoke": "Today a man knocked on my door and asked for a small donation towards the local swimming pool. I gave him five dollars.",
            "id": "e28"
        },
        {
            "joke": "My name is Joe, but all my friends call me infrequently.",
            "nonJoke": "My name is Joseph, but all my friends call me Joe.",
            "id": "e29"
        },
        {
            "joke": "Atheism is a non-prophet organization.",
            "nonJoke": "Atheism is a non-profit organization.",
            "id": "e30"
        },
        {
            "joke": "I am known at the gym as the \"before picture.\"",
            "nonJoke": "I am known at the gym as a bad athlete.",
            "id": "e31"
        },
        {
            "joke": "I don't suffer from insanity. I enjoy every minute of it.",
            "nonJoke": "I don't suffer from anything. I am a very healthy person.",
            "id": "e32"
        },
        {
            "joke": "A camel can work 10 days without drinking, but I can drink 10 days without working.",
            "nonJoke": "A camel can work 10 days without drinking, but I get thirsty after a couple of hours.",
            "id": "e33"
        },
        {
            "joke": "I was going to give him a nasty look, but he already had one.",
            "nonJoke": "I was going to give him a nasty look, but I changed my mind.",
            "id": "e34"
        },
        {
            "joke": "My conscience is clean \u2014 I have never used it.",
            "nonJoke": "My concience is clean - I was honest.",
            "id": "e35"
        },
        {
            "joke": "To steal ideas from one person is plagiarism. To steal from many is research.",
            "nonJoke": "To steal ideas from one person is plagiarism. Plagiarism is wrong.",
            "id": "e36"
        },
        {

            "joke": "The human brain is a wonderful thing. It starts working the moment you are born, and never stops until you stand up to speak in public.",
            "nonJoke": "The human brain is a wonderful thing. It starts working the moment you are born, and never stops until you die.",
            "id": "e37"
        },
        {
            "joke": "Hospitality: making your guests feel like they're at home, even if you wish they were.",
            "nonJoke": "Hospitality: making your guests feel like they're at home, even if they are very shy.",
            "id": "e38"
        },
        {
            "joke": "Yesterday, I fell down from a 10 meter ladder. Thank God I was on the third step.",
            "nonJoke": "Yesterday, I fell down from a 10 meter ladder. Thank God I didn't break anything.",
            "id": "p39"
        }
    ]
}
